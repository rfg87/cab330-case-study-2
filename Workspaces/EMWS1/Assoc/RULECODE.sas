data WORK.RULEID;
  length   SET_SIZE                             8
           EXP_CONF                             8
           CONF                                 8
           SUPPORT                              8
           LIFT                                 8
           COUNT                                8
           RULE                             $  67
           _LHAND                           $  48
           _RHAND                           $  48
           ITEM1                            $  14
           ITEM2                            $  14
           ITEM3                            $  14
           ITEM4                            $  14
           ITEM5                            $  14
           index                                8
           ruleid                               8
           ;
 
  label    SET_SIZE="Relations"
           EXP_CONF="Expected Confidence(%)"
           CONF="Confidence(%)"
           SUPPORT="Support(%)"
           LIFT="Lift"
           COUNT="Transaction Count"
           RULE="Rule"
           _LHAND="Left Hand of Rule"
           _RHAND="Right Hand of Rule"
           ITEM1="Rule Item 1"
           ITEM2="Rule Item 2"
           ITEM3="Rule Item 3"
           ITEM4="Rule Item 4"
           ITEM5="Rule Item 5"
           index="Rule Index"
           ;
  format   SET_SIZE 6.
           EXP_CONF 6.2
           CONF 6.2
           SUPPORT 6.2
           LIFT 6.2
           COUNT 6.2
           ;
SET_SIZE=2; EXP_CONF=6.735; CONF=24.2552245442418; SUPPORT=2.182; LIFT=3.6013696427976; COUNT=4364; RULE="Perfume ==> Toothbrush"; _LHAND="Perfume"; _RHAND="Toothbrush"; ITEM1="Perfume"; ITEM2="=============>"; ITEM3="Toothbrush"; ITEM4=""; ITEM5=""; index=1; ruleid=1;
output;
SET_SIZE=2; EXP_CONF=8.996; CONF=32.3979213066072; SUPPORT=2.182; LIFT=3.6013696427976; COUNT=4364; RULE="Toothbrush ==> Perfume"; _LHAND="Toothbrush"; _RHAND="Perfume"; ITEM1="Toothbrush"; ITEM2="=============>"; ITEM3="Perfume"; ITEM4=""; ITEM5=""; index=2; ruleid=2;
output;
SET_SIZE=3; EXP_CONF=14.6885; CONF=41.112618724559; SUPPORT=1.6665; LIFT=2.79896645161582; COUNT=3333; RULE="Magazine & Candy Bar ==> Greeting Cards"; _LHAND="Magazine & Candy Bar"; _RHAND="Greeting Cards"; ITEM1="Magazine"; ITEM2="Candy Bar"; ITEM3="=============>"; ITEM4="Greeting Cards";
ITEM5=""; index=3; ruleid=3;
output;
SET_SIZE=3; EXP_CONF=4.0535; CONF=11.3456105116247; SUPPORT=1.6665; LIFT=2.79896645161582; COUNT=3333; RULE="Greeting Cards ==> Magazine & Candy Bar"; _LHAND="Greeting Cards"; _RHAND="Magazine & Candy Bar"; ITEM1="Greeting Cards"; ITEM2="=============>"; ITEM3="Magazine"; ITEM4="Candy Bar";
ITEM5=""; index=4; ruleid=4;
output;
SET_SIZE=3; EXP_CONF=17.1005; CONF=45.8648685840099; SUPPORT=1.6665; LIFT=2.6820776342218; COUNT=3333; RULE="Magazine & Greeting Cards ==> Candy Bar"; _LHAND="Magazine & Greeting Cards"; _RHAND="Candy Bar"; ITEM1="Magazine"; ITEM2="Greeting Cards"; ITEM3="=============>"; ITEM4="Candy Bar";
ITEM5=""; index=5; ruleid=5;
output;
SET_SIZE=3; EXP_CONF=17.1005; CONF=43.3285962419074; SUPPORT=1.372; LIFT=2.53376195093169; COUNT=2744; RULE="Toothpaste & Magazine ==> Candy Bar"; _LHAND="Toothpaste & Magazine"; _RHAND="Candy Bar"; ITEM1="Toothpaste"; ITEM2="Magazine"; ITEM3="=============>"; ITEM4="Candy Bar"; ITEM5=""; index=6;
ruleid=6;
output;
SET_SIZE=3; EXP_CONF=17.1005; CONF=41.0692019950124; SUPPORT=1.3175; LIFT=2.40163749568798; COUNT=2635; RULE="Toothpaste & Greeting Cards ==> Candy Bar"; _LHAND="Toothpaste & Greeting Cards"; _RHAND="Candy Bar"; ITEM1="Toothpaste"; ITEM2="Greeting Cards"; ITEM3="=============>"; ITEM4="Candy Bar";
ITEM5=""; index=7; ruleid=7;
output;
SET_SIZE=3; EXP_CONF=14.6885; CONF=33.1196581196581; SUPPORT=1.3175; LIFT=2.25480192801566; COUNT=2635; RULE="Toothpaste & Candy Bar ==> Greeting Cards"; _LHAND="Toothpaste & Candy Bar"; _RHAND="Greeting Cards"; ITEM1="Toothpaste"; ITEM2="Candy Bar"; ITEM3="=============>"; ITEM4="Greeting Cards";
ITEM5=""; index=8; ruleid=8;
output;
SET_SIZE=3; EXP_CONF=16.0425; CONF=33.8472924633033; SUPPORT=1.372; LIFT=2.10985148594691; COUNT=2744; RULE="Magazine & Candy Bar ==> Toothpaste"; _LHAND="Magazine & Candy Bar"; _RHAND="Toothpaste"; ITEM1="Magazine"; ITEM2="Candy Bar"; ITEM3="=============>"; ITEM4="Toothpaste"; ITEM5=""; index=9;
ruleid=9;
output;
SET_SIZE=3; EXP_CONF=16.0425; CONF=30.1763628034814; SUPPORT=1.3175; LIFT=1.88102619937549; COUNT=2635; RULE="Greeting Cards & Candy Bar ==> Toothpaste"; _LHAND="Greeting Cards & Candy Bar"; _RHAND="Toothpaste"; ITEM1="Greeting Cards"; ITEM2="Candy Bar"; ITEM3="=============>"; ITEM4="Toothpaste";
ITEM5=""; index=10; ruleid=10;
output;
SET_SIZE=2; EXP_CONF=14.6885; CONF=25.5314172100231; SUPPORT=4.366; LIFT=1.73819091193948; COUNT=8732; RULE="Candy Bar ==> Greeting Cards"; _LHAND="Candy Bar"; _RHAND="Greeting Cards"; ITEM1="Candy Bar"; ITEM2="=============>"; ITEM3="Greeting Cards"; ITEM4=""; ITEM5=""; index=11; ruleid=11;
output;
SET_SIZE=2; EXP_CONF=17.1005; CONF=29.7239336896211; SUPPORT=4.366; LIFT=1.73819091193948; COUNT=8732; RULE="Greeting Cards ==> Candy Bar"; _LHAND="Greeting Cards"; _RHAND="Candy Bar"; ITEM1="Greeting Cards"; ITEM2="=============>"; ITEM3="Candy Bar"; ITEM4=""; ITEM5=""; index=12; ruleid=12;
output;
SET_SIZE=3; EXP_CONF=24.1305; CONF=38.1699496106275; SUPPORT=1.6665; LIFT=1.58181345644008; COUNT=3333; RULE="Greeting Cards & Candy Bar ==> Magazine"; _LHAND="Greeting Cards & Candy Bar"; _RHAND="Magazine"; ITEM1="Greeting Cards"; ITEM2="Candy Bar"; ITEM3="=============>"; ITEM4="Magazine";
ITEM5=""; index=13; ruleid=13;
output;
SET_SIZE=2; EXP_CONF=14.6885; CONF=21.6712988697424; SUPPORT=2.924; LIFT=1.47539223676634; COUNT=5848; RULE="Pencils ==> Greeting Cards"; _LHAND="Pencils"; _RHAND="Greeting Cards"; ITEM1="Pencils"; ITEM2="=============>"; ITEM3="Greeting Cards"; ITEM4=""; ITEM5=""; index=14; ruleid=14;
output;
SET_SIZE=2; EXP_CONF=13.4925; CONF=19.9067297545699; SUPPORT=2.924; LIFT=1.47539223676634; COUNT=5848; RULE="Greeting Cards ==> Pencils"; _LHAND="Greeting Cards"; _RHAND="Pencils"; ITEM1="Greeting Cards"; ITEM2="=============>"; ITEM3="Pencils"; ITEM4=""; ITEM5=""; index=15; ruleid=15;
output;
SET_SIZE=2; EXP_CONF=16.0425; CONF=23.2624777053302; SUPPORT=3.978; LIFT=1.45005315289576; COUNT=7956; RULE="Candy Bar ==> Toothpaste"; _LHAND="Candy Bar"; _RHAND="Toothpaste"; ITEM1="Candy Bar"; ITEM2="=============>"; ITEM3="Toothpaste"; ITEM4=""; ITEM5=""; index=16; ruleid=16;
output;
SET_SIZE=2; EXP_CONF=17.1005; CONF=24.7966339410939; SUPPORT=3.978; LIFT=1.45005315289576; COUNT=7956; RULE="Toothpaste ==> Candy Bar"; _LHAND="Toothpaste"; _RHAND="Candy Bar"; ITEM1="Toothpaste"; ITEM2="=============>"; ITEM3="Candy Bar"; ITEM4=""; ITEM5=""; index=17; ruleid=17;
output;
SET_SIZE=2; EXP_CONF=17.1005; CONF=24.4691495275152; SUPPORT=3.3015; LIFT=1.4309025775571; COUNT=6603; RULE="Pencils ==> Candy Bar"; _LHAND="Pencils"; _RHAND="Candy Bar"; ITEM1="Pencils"; ITEM2="=============>"; ITEM3="Candy Bar"; ITEM4=""; ITEM5=""; index=18; ruleid=18;
output;
SET_SIZE=2; EXP_CONF=13.4925; CONF=19.3064530276892; SUPPORT=3.3015; LIFT=1.4309025775571; COUNT=6603; RULE="Candy Bar ==> Pencils"; _LHAND="Candy Bar"; _RHAND="Pencils"; ITEM1="Candy Bar"; ITEM2="=============>"; ITEM3="Pencils"; ITEM4=""; ITEM5=""; index=19; ruleid=19;
output;
SET_SIZE=3; EXP_CONF=24.1305; CONF=34.4896933132227; SUPPORT=1.372; LIFT=1.42929874280361; COUNT=2744; RULE="Toothpaste & Candy Bar ==> Magazine"; _LHAND="Toothpaste & Candy Bar"; _RHAND="Magazine"; ITEM1="Toothpaste"; ITEM2="Candy Bar"; ITEM3="=============>"; ITEM4="Magazine"; ITEM5=""; index=20;
ruleid=20;
output;
SET_SIZE=2; EXP_CONF=14.6885; CONF=19.9968832787907; SUPPORT=3.208; LIFT=1.36139723448893; COUNT=6416; RULE="Toothpaste ==> Greeting Cards"; _LHAND="Toothpaste"; _RHAND="Greeting Cards"; ITEM1="Toothpaste"; ITEM2="=============>"; ITEM3="Greeting Cards"; ITEM4=""; ITEM5=""; index=21; ruleid=21;
output;
SET_SIZE=2; EXP_CONF=16.0425; CONF=21.8402151342887; SUPPORT=3.208; LIFT=1.36139723448893; COUNT=6416; RULE="Greeting Cards ==> Toothpaste"; _LHAND="Greeting Cards"; _RHAND="Toothpaste"; ITEM1="Greeting Cards"; ITEM2="=============>"; ITEM3="Toothpaste"; ITEM4=""; ITEM5=""; index=22; ruleid=22;
output;
SET_SIZE=2; EXP_CONF=24.1305; CONF=28.1378248974008; SUPPORT=1.6455; LIFT=1.16606887123767; COUNT=3291; RULE="Photo Processi ==> Magazine"; _LHAND="Photo Processi"; _RHAND="Magazine"; ITEM1="Photo Processi"; ITEM2="=============>"; ITEM3="Magazine"; ITEM4=""; ITEM5=""; index=23; ruleid=23;
output;
SET_SIZE=2; EXP_CONF=13.4925; CONF=15.3093345800218; SUPPORT=2.456; LIFT=1.13465514767625; COUNT=4912; RULE="Toothpaste ==> Pencils"; _LHAND="Toothpaste"; _RHAND="Pencils"; ITEM1="Toothpaste"; ITEM2="=============>"; ITEM3="Pencils"; ITEM4=""; ITEM5=""; index=24; ruleid=24;
output;
SET_SIZE=2; EXP_CONF=16.0425; CONF=18.2027052065962; SUPPORT=2.456; LIFT=1.13465514767625; COUNT=4912; RULE="Pencils ==> Toothpaste"; _LHAND="Pencils"; _RHAND="Toothpaste"; ITEM1="Pencils"; ITEM2="=============>"; ITEM3="Toothpaste"; ITEM4=""; ITEM5=""; index=25; ruleid=25;
output;
SET_SIZE=2; EXP_CONF=14.6885; CONF=15.0577070512422; SUPPORT=3.6335; LIFT=1.02513578998823; COUNT=7267; RULE="Magazine ==> Greeting Cards"; _LHAND="Magazine"; _RHAND="Greeting Cards"; ITEM1="Magazine"; ITEM2="=============>"; ITEM3="Greeting Cards"; ITEM4=""; ITEM5=""; index=26; ruleid=26;
output;
SET_SIZE=2; EXP_CONF=24.1305; CONF=24.7370391803111; SUPPORT=3.6335; LIFT=1.02513578998823; COUNT=7267; RULE="Greeting Cards ==> Magazine"; _LHAND="Greeting Cards"; _RHAND="Magazine"; ITEM1="Greeting Cards"; ITEM2="=============>"; ITEM3="Magazine"; ITEM4=""; ITEM5=""; index=27; ruleid=27;
output;
SET_SIZE=2; EXP_CONF=24.1305; CONF=23.7039852635887; SUPPORT=4.0535; LIFT=0.98232466229828; COUNT=8107; RULE="Candy Bar ==> Magazine"; _LHAND="Candy Bar"; _RHAND="Magazine"; ITEM1="Candy Bar"; ITEM2="=============>"; ITEM3="Magazine"; ITEM4=""; ITEM5=""; index=28; ruleid=28;
output;
SET_SIZE=2; EXP_CONF=17.1005; CONF=16.7982428876318; SUPPORT=4.0535; LIFT=0.98232466229828; COUNT=8107; RULE="Magazine ==> Candy Bar"; _LHAND="Magazine"; _RHAND="Candy Bar"; ITEM1="Magazine"; ITEM2="=============>"; ITEM3="Candy Bar"; ITEM4=""; ITEM5=""; index=29; ruleid=29;
output;
SET_SIZE=2; EXP_CONF=24.1305; CONF=23.4426533259218; SUPPORT=3.163; LIFT=0.97149471937679; COUNT=6326; RULE="Pencils ==> Magazine"; _LHAND="Pencils"; _RHAND="Magazine"; ITEM1="Pencils"; ITEM2="=============>"; ITEM3="Magazine"; ITEM4=""; ITEM5=""; index=30; ruleid=30;
output;
SET_SIZE=2; EXP_CONF=13.4925; CONF=13.1078925011914; SUPPORT=3.163; LIFT=0.97149471937679; COUNT=6326; RULE="Magazine ==> Pencils"; _LHAND="Magazine"; _RHAND="Pencils"; ITEM1="Magazine"; ITEM2="=============>"; ITEM3="Pencils"; ITEM4=""; ITEM5=""; index=31; ruleid=31;
output;
SET_SIZE=2; EXP_CONF=24.1305; CONF=19.7381954184198; SUPPORT=3.1665; LIFT=0.81797705884336; COUNT=6333; RULE="Toothpaste ==> Magazine"; _LHAND="Toothpaste"; _RHAND="Magazine"; ITEM1="Toothpaste"; ITEM2="=============>"; ITEM3="Magazine"; ITEM4=""; ITEM5=""; index=32; ruleid=32;
output;
SET_SIZE=2; EXP_CONF=16.0425; CONF=13.1223969664946; SUPPORT=3.1665; LIFT=0.81797705884336; COUNT=6333; RULE="Magazine ==> Toothpaste"; _LHAND="Magazine"; _RHAND="Toothpaste"; ITEM1="Magazine"; ITEM2="=============>"; ITEM3="Toothpaste"; ITEM4=""; ITEM5=""; index=33; ruleid=33;
output;
SET_SIZE=2; EXP_CONF=24.1305; CONF=19.561989606533; SUPPORT=1.3175; LIFT=0.8106748557441; COUNT=2635; RULE="Toothbrush ==> Magazine"; _LHAND="Toothbrush"; _RHAND="Magazine"; ITEM1="Toothbrush"; ITEM2="=============>"; ITEM3="Magazine"; ITEM4=""; ITEM5=""; index=34; ruleid=34;
output;
SET_SIZE=2; EXP_CONF=24.1305; CONF=19.1084926634059; SUPPORT=1.719; LIFT=0.79188133952491; COUNT=3438; RULE="Perfume ==> Magazine"; _LHAND="Perfume"; _RHAND="Magazine"; ITEM1="Perfume"; ITEM2="=============>"; ITEM3="Magazine"; ITEM4=""; ITEM5=""; index=35; ruleid=35;
output;
SET_SIZE=2; EXP_CONF=24.1305; CONF=14.7309768413721; SUPPORT=2.115; LIFT=0.61047126422461; COUNT=4230; RULE="Pens ==> Magazine"; _LHAND="Pens"; _RHAND="Magazine"; ITEM1="Pens"; ITEM2="=============>"; ITEM3="Magazine"; ITEM4=""; ITEM5=""; index=36; ruleid=36;
output;
;
run;
