*****************************************;
*** Begin Scoring Code from PROC DMVQ ***;
*****************************************;


*** Begin Class Look-up, Standardization, Replacement ;
drop _dm_bad; _dm_bad = 0;

*** Standardize HATCH ;
drop T_HATCH ;
if missing( HATCH ) then T_HATCH = .;
else T_HATCH = (HATCH - 1914.07810107197) * 0.00283792585438;

*** Standardize SEDAN ;
drop T_SEDAN ;
if missing( SEDAN ) then T_SEDAN = .;
else T_SEDAN = (SEDAN - 1850.85758039816) * 0.00344741137786;

*** Standardize WAG0N ;
drop T_WAG0N ;
if missing( WAG0N ) then T_WAG0N = .;
else T_WAG0N = (WAG0N - 446.993874425727) * 0.00471598536736;

*** End Class Look-up, Standardization, Replacement ;


*** Omitted Cases;
if _dm_bad then do;
   _SEGMENT_ = .; Distance = .;
   goto CLUSvlex ;
end; *** omitted;

*** Compute Distances and Cluster Membership;
label _SEGMENT_ = 'Segment Id' ;
label Distance = 'Distance' ;
array CLUSvads [20] _temporary_;
drop _vqclus _vqmvar _vqnvar;
_vqmvar = 0;
do _vqclus = 1 to 20; CLUSvads [_vqclus] = 0; end;
if not missing( T_HATCH ) then do;
   CLUSvads [1] + ( T_HATCH - -1.17275801055362 )**2;
   CLUSvads [2] + ( T_HATCH - -0.05212689763446 )**2;
   CLUSvads [3] + ( T_HATCH - -0.13150199290783 )**2;
   CLUSvads [4] + ( T_HATCH - 0.18150346914126 )**2;
   CLUSvads [5] + ( T_HATCH - -0.559822143185 )**2;
   CLUSvads [6] + ( T_HATCH - 0.90622394911846 )**2;
   CLUSvads [7] + ( T_HATCH - 1.641589409855 )**2;
   CLUSvads [8] + ( T_HATCH - -1.48847726185363 )**2;
   CLUSvads [9] + ( T_HATCH - 0.71871957139205 )**2;
   CLUSvads [10] + ( T_HATCH - 1.96818840516414 )**2;
   CLUSvads [11] + ( T_HATCH - -1.09897193833969 )**2;
   CLUSvads [12] + ( T_HATCH - -0.3672600555515 )**2;
   CLUSvads [13] + ( T_HATCH - 0.45384649164972 )**2;
   CLUSvads [14] + ( T_HATCH - -2.09366495030062 )**2;
   CLUSvads [15] + ( T_HATCH - -0.77417221673517 )**2;
   CLUSvads [16] + ( T_HATCH - -1.57138148472933 )**2;
   CLUSvads [17] + ( T_HATCH - -2.46827116307906 )**2;
   CLUSvads [18] + ( T_HATCH - 0.71691475012501 )**2;
   CLUSvads [19] + ( T_HATCH - -3.45066649633767 )**2;
   CLUSvads [20] + ( T_HATCH - -0.82566391230794 )**2;
end;
else _vqmvar + 1;
if not missing( T_SEDAN ) then do;
   CLUSvads [1] + ( T_SEDAN - -0.19514961673775 )**2;
   CLUSvads [2] + ( T_SEDAN - 0.82881899683321 )**2;
   CLUSvads [3] + ( T_SEDAN - -0.62068147857341 )**2;
   CLUSvads [4] + ( T_SEDAN - 0.00401764116993 )**2;
   CLUSvads [5] + ( T_SEDAN - 0.79637556163091 )**2;
   CLUSvads [6] + ( T_SEDAN - -0.77759710394502 )**2;
   CLUSvads [7] + ( T_SEDAN - -1.09061472213689 )**2;
   CLUSvads [8] + ( T_SEDAN - 2.33094107038917 )**2;
   CLUSvads [9] + ( T_SEDAN - -1.05809414147241 )**2;
   CLUSvads [10] + ( T_SEDAN - -1.74225041627804 )**2;
   CLUSvads [11] + ( T_SEDAN - -0.23718888326221 )**2;
   CLUSvads [12] + ( T_SEDAN - -1.22350422263106 )**2;
   CLUSvads [13] + ( T_SEDAN - -2.35236728643365 )**2;
   CLUSvads [14] + ( T_SEDAN - 0.59114746169582 )**2;
   CLUSvads [15] + ( T_SEDAN - 0.34308561531785 )**2;
   CLUSvads [16] + ( T_SEDAN - 1.25917917758336 )**2;
   CLUSvads [17] + ( T_SEDAN - -3.30327825816004 )**2;
   CLUSvads [18] + ( T_SEDAN - -0.0888695528124 )**2;
   CLUSvads [19] + ( T_SEDAN - -2.49486029005187 )**2;
   CLUSvads [20] + ( T_SEDAN - 1.54560160622723 )**2;
end;
else _vqmvar + 1;
if not missing( T_WAG0N ) then do;
   CLUSvads [1] + ( T_WAG0N - 2.66063063287626 )**2;
   CLUSvads [2] + ( T_WAG0N - -0.64325885503617 )**2;
   CLUSvads [3] + ( T_WAG0N - 1.39072057311848 )**2;
   CLUSvads [4] + ( T_WAG0N - 0.18704210096259 )**2;
   CLUSvads [5] + ( T_WAG0N - 0.29713596626291 )**2;
   CLUSvads [6] + ( T_WAG0N - -0.03338436990974 )**2;
   CLUSvads [7] + ( T_WAG0N - -0.75217077797679 )**2;
   CLUSvads [8] + ( T_WAG0N - -0.30454516685731 )**2;
   CLUSvads [9] + ( T_WAG0N - 0.77942406983256 )**2;
   CLUSvads [10] + ( T_WAG0N - -0.31061101760157 )**2;
   CLUSvads [11] + ( T_WAG0N - -2.01317286537398 )**2;
   CLUSvads [12] + ( T_WAG0N - -1.98764665886171 )**2;
   CLUSvads [13] + ( T_WAG0N - -1.8486373758902 )**2;
   CLUSvads [14] + ( T_WAG0N - 3.15659509401128 )**2;
   CLUSvads [15] + ( T_WAG0N - 1.2486082593437 )**2;
   CLUSvads [16] + ( T_WAG0N - 1.37342862677146 )**2;
   CLUSvads [17] + ( T_WAG0N - -0.95574414633491 )**2;
   CLUSvads [18] + ( T_WAG0N - -0.64227590842928 )**2;
   CLUSvads [19] + ( T_WAG0N - -0.38589591444443 )**2;
   CLUSvads [20] + ( T_WAG0N - -0.36624597541372 )**2;
end;
else _vqmvar + 1;
_vqnvar = 3 - _vqmvar;
if _vqnvar <= 1.0231815394945E-12 then do;
   _SEGMENT_ = .; Distance = .;
end;
else do;
   _SEGMENT_ = 1; Distance = CLUSvads [1];
   _vqfzdst = Distance * 0.99999999999988; drop _vqfzdst;
   do _vqclus = 2 to 20;
      if CLUSvads [_vqclus] < _vqfzdst then do;
         _SEGMENT_ = _vqclus; Distance = CLUSvads [_vqclus];
         _vqfzdst = Distance * 0.99999999999988;
      end;
   end;
   Distance = sqrt(Distance * (3 / _vqnvar));
end;
CLUSvlex :;

***************************************;
*** End Scoring Code from PROC DMVQ ***;
***************************************;
*------------------------------------------------------------*;
* Clus: Creating Segment Label;
*------------------------------------------------------------*;
length _SEGMENT_LABEL_ $80;
label _SEGMENT_LABEL_='Segment Description';
if _SEGMENT_ = 1 then _SEGMENT_LABEL_="Cluster1";
else
if _SEGMENT_ = 2 then _SEGMENT_LABEL_="Cluster2";
else
if _SEGMENT_ = 3 then _SEGMENT_LABEL_="Cluster3";
else
if _SEGMENT_ = 4 then _SEGMENT_LABEL_="Cluster4";
else
if _SEGMENT_ = 5 then _SEGMENT_LABEL_="Cluster5";
else
if _SEGMENT_ = 6 then _SEGMENT_LABEL_="Cluster6";
else
if _SEGMENT_ = 7 then _SEGMENT_LABEL_="Cluster7";
else
if _SEGMENT_ = 8 then _SEGMENT_LABEL_="Cluster8";
else
if _SEGMENT_ = 9 then _SEGMENT_LABEL_="Cluster9";
else
if _SEGMENT_ = 10 then _SEGMENT_LABEL_="Cluster10";
else
if _SEGMENT_ = 11 then _SEGMENT_LABEL_="Cluster11";
else
if _SEGMENT_ = 12 then _SEGMENT_LABEL_="Cluster12";
else
if _SEGMENT_ = 13 then _SEGMENT_LABEL_="Cluster13";
else
if _SEGMENT_ = 14 then _SEGMENT_LABEL_="Cluster14";
else
if _SEGMENT_ = 15 then _SEGMENT_LABEL_="Cluster15";
else
if _SEGMENT_ = 16 then _SEGMENT_LABEL_="Cluster16";
else
if _SEGMENT_ = 17 then _SEGMENT_LABEL_="Cluster17";
else
if _SEGMENT_ = 18 then _SEGMENT_LABEL_="Cluster18";
else
if _SEGMENT_ = 19 then _SEGMENT_LABEL_="Cluster19";
else
if _SEGMENT_ = 20 then _SEGMENT_LABEL_="Cluster20";
